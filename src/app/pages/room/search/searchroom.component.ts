import { Component, OnInit, OnDestroy } from "@angular/core";
import Swal from "sweetalert2";
import {RoomService} from "../../../services/room/room.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {DatePipe} from "@angular/common";
import {GLOBAL} from "../../../services/global";
import {Router} from "@angular/router";

@Component({
  selector: "app-searchroom",
  templateUrl: "searchroom.component.html",
  providers: [DatePipe]
})
export class SearchroomComponent implements OnInit, OnDestroy {

  isCollapsed = true;
  date = new Date();
  rooms: any = [];
  hotel: any = [];
  formSearchHotel: FormGroup;
  public global = GLOBAL;
  
  constructor(
      private _roomService: RoomService,
      private datepipe: DatePipe,
      private router: Router
  ) {

    this.formSearchHotel = new FormGroup({
      data: new FormGroup({
        fecha_entrada: new FormControl('', [Validators.required]),
        fecha_salida: new FormControl('', [Validators.required]),
        cant_personas: new FormControl('', [Validators.required]),
        ciudad: new FormControl('', [Validators.required]),
      })
    });
    
  }

  ngOnInit() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.add("profile-page");
  }
  ngOnDestroy() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.remove("profile-page");
  }

  searchHotel() {
    const data = this.formSearchHotel.value.data;
    data.fecha_entrada = this.datepipe.transform(data.fecha_entrada, 'yyyy-MM-dd');
    data.fecha_salida = this.datepipe.transform(data.fecha_salida, 'yyyy-MM-dd');

    this._roomService.searchRoom(data).subscribe((resp: any) => {
      if (resp.success){
        this.rooms = resp.rooms;
      }
    }, error => {
      this.rooms = [];
      Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: error.error.message,
      });
    });
  }

  reserve(cod_hotel, cod_habitacion) {
    const data = this.formSearchHotel.value.data;
    data.fecha_entrada = this.datepipe.transform(data.fecha_entrada, 'yyyy-MM-dd');
    data.fecha_salida = this.datepipe.transform(data.fecha_salida, 'yyyy-MM-dd');
    data.cod_habitacion = cod_habitacion;
    data.cod_hotel = cod_hotel;

    localStorage.setItem('reserve', JSON.stringify(data));
    this.router.navigate(['/createReserve']);
  }

}
