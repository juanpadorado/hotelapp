import { Component, OnInit, OnDestroy } from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {RoomService} from "../../../services/room/room.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import Swal from "sweetalert2";

@Component({
  selector: "app-createroom",
  templateUrl: "createroom.component.html"
})
export class CreateroomComponent implements OnInit, OnDestroy {
  
  isCollapsed = true;
  cod_hotel: number;
  formCreateRoom: FormGroup;
  hotel: any = [];
  
  constructor(
      private _route: ActivatedRoute,
      private _roomService: RoomService
  ) {
    this._route.params
        .subscribe(params => {
          this.cod_hotel = params.id;
        });

    this.getHotel();

    this.formCreateRoom = new FormGroup({
      data: new FormGroup({
        identificador: new FormControl('', [Validators.required]),
        costo_base: new FormControl('', [Validators.required]),
        impuesto: new FormControl('', [Validators.required]),
        tipo_habitacion: new FormControl('', [Validators.required]),
        ubicacion: new FormControl('', [Validators.required]),
        cant_personas: new FormControl('', [Validators.required]),
      })
    });
    
  }

  ngOnInit() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.add("profile-page");
  }
  ngOnDestroy() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.remove("profile-page");
  }

  getHotel(){
    this._roomService.listRooms(this.cod_hotel).subscribe((resp: any) => {
      if (resp.success){
        this.hotel = resp.hotel;
      }
    }, error => {
      Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: error.error.message,
      });
    });
  }

  createRoom() {
    const data = this.formCreateRoom.value.data;
    data.cod_hotel = this.cod_hotel;

    this._roomService.createRoom(data).subscribe((resp: any) => {
      if (resp.success){
        this.formCreateRoom.reset({});
        Swal.fire({
          type: 'success',
          title: 'Éxito',
          text: resp.message,
        });
      }
    }, error => {
      Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: error.error.message,
      });
    });
  }

}
