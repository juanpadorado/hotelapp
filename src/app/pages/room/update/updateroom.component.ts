import { Component, OnInit, OnDestroy } from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {RoomService} from "../../../services/room/room.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import Swal from "sweetalert2";

@Component({
  selector: "app-updateroom",
  templateUrl: "updateroom.component.html"
})
export class UpdateroomComponent implements OnInit, OnDestroy {

  isCollapsed = true;
  cod_hotel: number;
  cod_habitacion: number;
  formUpdateRoom: FormGroup;
  room: any = [];

  constructor(
      private _route: ActivatedRoute,
      private _roomService: RoomService
  ) {

    this._route.params
        .subscribe(params => {
          this.cod_habitacion = params.id;
          this.cod_hotel = params.cod_hotel;
        });

    this.formUpdateRoom = new FormGroup({
      data: new FormGroup({
        identificador: new FormControl('', [Validators.required]),
        costo_base: new FormControl('', [Validators.required]),
        impuesto: new FormControl('', [Validators.required]),
        tipo_habitacion: new FormControl('', [Validators.required]),
        ubicacion: new FormControl('', [Validators.required]),
        cant_personas: new FormControl('', [Validators.required]),
      })
    });
    this.getRoom();
  }

  ngOnInit() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.add("profile-page");
  }
  ngOnDestroy() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.remove("profile-page");
  }

  getRoom() {
    this._roomService.getRoom(this.cod_habitacion).subscribe((resp: any) => {
      if (resp.success){
        this.room = resp.room;
        this.formUpdateRoom.get('data.identificador').setValue(this.room.identificador);
        this.formUpdateRoom.get('data.costo_base').setValue(this.room.costo_base);
        this.formUpdateRoom.get('data.impuesto').setValue(this.room.impuesto);
        this.formUpdateRoom.get('data.tipo_habitacion').setValue(this.room.tipo_habitacion);
        this.formUpdateRoom.get('data.ubicacion').setValue(this.room.ubicacion);
        this.formUpdateRoom.get('data.cant_personas').setValue(this.room.cant_personas);
      }
    }, error => {
      Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: error.error.message,
      });
    });
  }

  updateRoom() {
    const data = this.formUpdateRoom.value.data;

    this._roomService.updateRoom(this.cod_habitacion, data).subscribe((resp: any) => {

      if (resp.success){
        this.room = resp.room;
        Swal.fire({
          type: 'success',
          title: 'Éxito',
          text: resp.message,
        });
      }
    }, error => {
      Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: error.error.message,
      });
    });
  }

}
