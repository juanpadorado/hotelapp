import { Component, OnInit, OnDestroy } from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {Subject} from "rxjs";
import {DataTableDirective} from "angular-datatables";
import {GLOBAL} from "../../../services/global";
import Swal from "sweetalert2";
import {RoomService} from "../../../services/room/room.service";

@Component({
  selector: "app-listroom",
  templateUrl: "listroom.component.html"
})
export class ListroomComponent implements OnInit, OnDestroy {

  isCollapsed = true;
  cod_hotel: number;
  dtOptions: DataTables.Settings = {};
  rooms: any = [];
  hotel: any = [];
  dtTrigger: Subject<any> = new Subject();
  dtElement: DataTableDirective;
  isDtInitialized: boolean = false;

  constructor(
      private _route: ActivatedRoute,
      private _roomService: RoomService
  ) {

    this._route.params
        .subscribe(params => {
          this.cod_hotel = params.id;
        });

    this.listRooms();
  }

  ngOnInit() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.add("profile-page");
  }
  ngOnDestroy() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.remove("profile-page");
  }

  listRooms() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: GLOBAL.limit,
      processing: true,
    };

    this._roomService.listRooms(this.cod_hotel).subscribe((resp: any) => {
      if (resp.success){
        this.hotel = resp.hotel;
        this.rooms = resp.rooms;
        if (this.isDtInitialized) {
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.dtTrigger.next();
          });
        } else {
          this.isDtInitialized = true;
          this.dtTrigger.next();
        }
      }
    }, error => {
      Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: error.error.message,
      });
    });
  }

  activeRoom(e, cod_habitacion) {
    const data = {
      cod_habitacion: cod_habitacion,
      estado: e.target.checked
    };
    this._roomService.activeRoom(data).subscribe((resp: any) => {
      if (resp.success){
        this.listRooms();
        Swal.fire({
          type: 'success',
          title: 'Éxito',
          text: resp.message,
        });
      }
    }, error => {
      Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: error.error.message,
      });
    });
  }

}
