import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { ProgressbarModule } from "ngx-bootstrap/progressbar";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { CollapseModule } from "ngx-bootstrap/collapse";
import { TabsModule } from "ngx-bootstrap/tabs";
import { PaginationModule } from "ngx-bootstrap/pagination";
import { AlertModule } from "ngx-bootstrap/alert";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { CarouselModule } from "ngx-bootstrap/carousel";
import { ModalModule } from "ngx-bootstrap/modal";
import { JwBootstrapSwitchNg2Module } from "jw-bootstrap-switch-ng2";
import { PopoverModule } from "ngx-bootstrap/popover";

import { IndexComponent } from "./index/index.component";
import { ProfilepageComponent } from "./examples/profilepage/profilepage.component";
import { RegisterpageComponent } from "./examples/registerpage/registerpage.component";
import { LandingpageComponent } from "./examples/landingpage/landingpage.component";
import {LoginComponent} from "./login/login.component";
import {PagesRoutingModule} from "./pages-routing.module";
import {HeaderComponent} from "./header/header.component";
import {FooterComponent} from "./footer/footer.component";
import {CreatehotelComponent} from "./hotel/create/createhotel.component";
import {UpdatehotelComponent} from "./hotel/update/updatehotel.component";
import {ListhotelComponent} from "./hotel/list/listhotel.component";
import {CreateroomComponent} from "./room/create/createroom.component";
import {UpdateroomComponent} from "./room/update/updateroom.component";
import {ListroomComponent} from "./room/list/listroom.component";
import {CreatereserveComponent} from "./reserve/create/createreserve.component";
import {ListreserveComponent} from "./reserve/list/listreserve.component";
import {DataTablesModule} from "angular-datatables";
import {SearchroomComponent} from "./room/search/searchroom.component";
import {UpdatereserveComponent} from "./reserve/update/updatereserve.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PagesRoutingModule,
    BsDropdownModule.forRoot(),
    ProgressbarModule.forRoot(),
    TooltipModule.forRoot(),
    PopoverModule.forRoot(),
    CollapseModule.forRoot(),
    JwBootstrapSwitchNg2Module,
    TabsModule.forRoot(),
    PaginationModule.forRoot(),
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    CarouselModule.forRoot(),
    ModalModule.forRoot(),
    ReactiveFormsModule,
    DataTablesModule
  ],
  declarations: [
    IndexComponent,
    ProfilepageComponent,
    RegisterpageComponent,
    LandingpageComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    CreatehotelComponent,
    UpdatehotelComponent,
    ListhotelComponent,
    CreateroomComponent,
    UpdateroomComponent,
    ListroomComponent,
    CreatereserveComponent,
    ListreserveComponent,
    SearchroomComponent,
    UpdatereserveComponent
  ],
  exports: [
    IndexComponent,
    ProfilepageComponent,
    RegisterpageComponent,
    LandingpageComponent,
    HeaderComponent,
    FooterComponent,
  ],
  providers: []
})
export class PagesModule {}
