import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {IndexComponent} from './index/index.component';
import {ProfilepageComponent} from "./examples/profilepage/profilepage.component";
import {RegisterpageComponent} from "./examples/registerpage/registerpage.component";
import {LandingpageComponent} from "./examples/landingpage/landingpage.component";
import {CreatehotelComponent} from "./hotel/create/createhotel.component";
import {UpdatehotelComponent} from "./hotel/update/updatehotel.component";
import {ListhotelComponent} from "./hotel/list/listhotel.component";
import {ListroomComponent} from "./room/list/listroom.component";
import {CreateroomComponent} from "./room/create/createroom.component";
import {UpdateroomComponent} from "./room/update/updateroom.component";
import {CreatereserveComponent} from "./reserve/create/createreserve.component";
import {ListreserveComponent} from "./reserve/list/listreserve.component";
import {SearchroomComponent} from "./room/search/searchroom.component";

const routes: Routes = [
    { path: "home", component: IndexComponent },
    { path: "profile", component: ProfilepageComponent },
    { path: "register", component: RegisterpageComponent },
    { path: "landing", component: LandingpageComponent },

    { path: "createHotel", component: CreatehotelComponent },
    { path: "updateHotel/:id", component: UpdatehotelComponent },
    { path: "listHotel", component: ListhotelComponent },

    { path: "listRoom/:id", component: ListroomComponent },
    { path: "createRoom/:id", component: CreateroomComponent },
    { path: "updateRoom/:id/:cod_hotel", component: UpdateroomComponent },
    { path: "searchRoom", component: SearchroomComponent },

    { path: "listReserve", component: ListreserveComponent },
    { path: "createReserve", component: CreatereserveComponent },
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule {
}
