import {Component, OnInit, OnDestroy, TemplateRef} from "@angular/core";
import {Subject} from "rxjs";
import {DataTableDirective} from "angular-datatables";
import {GLOBAL} from "../../../services/global";
import Swal from "sweetalert2";
import {ReserveService} from "../../../services/reserve/reserve.service";
import {BsModalRef, BsModalService} from "ngx-bootstrap";

@Component({
  selector: "app-listreserve",
  templateUrl: "listreserve.component.html"
})
export class ListreserveComponent implements OnInit, OnDestroy {

  isCollapsed = true;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  dtElement: DataTableDirective;
  isDtInitialized: boolean = false;
  modalRef: BsModalRef;
  reserves: any = [];
  room: any = [];
  emergencyContacts: any = [];
  guests: any = [];
  cod_reserva: number;

  constructor(
      private _reseveService: ReserveService,
      private modalService: BsModalService
  ) {
    this.listReserves();
  }

  ngOnInit() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.add("profile-page");
  }
  ngOnDestroy() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.remove("profile-page");
  }

  listReserves() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: GLOBAL.limit,
      processing: true,
    };

    this._reseveService.listReserves().subscribe((resp: any) => {
      if (resp.success){
        this.reserves = resp.reserves;

        if (this.isDtInitialized) {
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.dtTrigger.next();
          });
        } else {
          this.isDtInitialized = true;
          this.dtTrigger.next();
        }
      }
    }, error => {
      Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: error.error.message,
      });
    });
  }

  openModal(cod_reserva) {

    this._reseveService.getReserve(cod_reserva).subscribe((resp: any) => {
      if (resp.success){
        this.cod_reserva = cod_reserva;
        this.room = resp.room;
        this.emergencyContacts = resp.emergencyContacts;
        this.guests = resp.guests;
      }
    }, error => {
      Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: error.error.message,
      });
    });
  }

}
