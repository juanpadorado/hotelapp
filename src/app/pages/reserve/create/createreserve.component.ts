import { Component, OnInit, OnDestroy } from "@angular/core";
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import Swal from "sweetalert2";
import {ReserveService} from "../../../services/reserve/reserve.service";
import {Router} from "@angular/router";

@Component({
  selector: "app-createreserve",
  templateUrl: "createreserve.component.html"
})
export class CreatereserveComponent implements OnInit, OnDestroy {
  
  isCollapsed = true;
  date = new Date();
  formCreateReserve: FormGroup;
  dataPersons: FormArray;
  jsonForm: string;
  dataForm: any = [];

  constructor(
      private formBuilder: FormBuilder,
      private _reserveService: ReserveService,
      private router: Router
  ) {
    this.jsonForm = localStorage.getItem('reserve');
    this.dataForm = JSON.parse(this.jsonForm);

    this.formCreateReserve = this.formBuilder.group({
      persons: this.formBuilder.array([ this.createDataPerson() ]),
      contacto_nombreCom: ['', [Validators.required]],
      contacto_telefono: ['', [Validators.required]],
    });

    for (let i = 0; i < this.dataForm.cant_personas - 1; i++) {
      this.addPerson();
    }
  }

  ngOnInit() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.add("profile-page");
  }
  ngOnDestroy() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.remove("profile-page");
  }

  createDataPerson(): FormGroup {
    return this.formBuilder.group({
      nombre: ['', [Validators.required]],
      apellido: ['', [Validators.required]],
      fecha_nacimiento: ['', [Validators.required]],
      genero: ['', [Validators.required]],
      tipo_documento: ['', [Validators.required]],
      num_documento: ['', [Validators.required]],
      email: ['', [Validators.required,
          Validators.pattern('^([a-zA-Z0-9_.+-])+\\@(([a-zA-Z0-9-])+\\.)+([a-zA-Z0-9]{2,4})+$')]],
      telefono: ['', [Validators.required]],
    });
  }

  addPerson(): void {
    this.dataPersons = this.formCreateReserve.get('persons') as FormArray;
    this.dataPersons.push(this.createDataPerson());
  }

  createReserve() {
    const data = this.formCreateReserve.value;
    data.reserve = this.dataForm;

    this._reserveService.createReserve(data).subscribe((resp: any) => {
      if (resp.success){
        this.formCreateReserve.reset({});
        localStorage.removeItem('reserve');
        Swal.fire({
          type: 'success',
          title: 'Éxito',
          text: resp.message,
        });
        this.router.navigate(['/searchRoom']);
      }
    }, error => {
      Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: error.error.message,
      });
    });
  }

  goBack() {
    localStorage.removeItem('reserve');
    this.router.navigate(['/searchRoom']);
  }
}
