import { Component, OnInit, OnDestroy } from "@angular/core";
import {HotelService} from "../../../services/hotel/hotel.service";
import Swal from "sweetalert2";
import {GLOBAL} from "../../../services/global";
import {Subject} from "rxjs";
import {DataTableDirective} from "angular-datatables";

@Component({
  selector: "app-listhotel",
  templateUrl: "listhotel.component.html"
})
export class ListhotelComponent implements OnInit, OnDestroy {

  isCollapsed = true;
  dtOptions: DataTables.Settings = {};
  hotels: any = [];
  dtTrigger: Subject<any> = new Subject();
  dtElement: DataTableDirective;
  isDtInitialized: boolean = false;

  constructor(
      private _hotelService: HotelService,
  ) {
    this.listHotel();
  }

  ngOnInit() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.add("profile-page");
  }
  ngOnDestroy() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.remove("profile-page");
    this.dtTrigger.unsubscribe();
  }

  listHotel() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: GLOBAL.limit,
      processing: true,
    };

    this._hotelService.listHotels().subscribe((resp: any) => {
      if (resp.success){
        this.hotels = resp.hotels;

        if (this.isDtInitialized) {
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.dtTrigger.next();
          });
        } else {
          this.isDtInitialized = true;
          this.dtTrigger.next();
        }
      }
    }, error => {
      Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: error.error.message,
      });
    });
  }

  activeHotel(e, cod_hotel) {
    const data = {
      cod_hotel: cod_hotel,
      estado: e.target.checked
    };
    this._hotelService.activeHotel(data).subscribe((resp: any) => {
      if (resp.success){
        this.listHotel();
        Swal.fire({
          type: 'success',
          title: 'Éxito',
          text: resp.message,
        });
      }
    }, error => {
      Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: error.error.message,
      });
    });
  }
}
