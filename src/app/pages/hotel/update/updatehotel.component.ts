import { Component, OnInit, OnDestroy } from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import Swal from "sweetalert2";
import {HotelService} from "../../../services/hotel/hotel.service";

@Component({
  selector: "app-updatehotel",
  templateUrl: "updatehotel.component.html"
})
export class UpdatehotelComponent implements OnInit, OnDestroy {
  
  isCollapsed = true;
  cod_hotel: number;
  formUpdateHotel: FormGroup;
  hotel: any = [];
  
  constructor(
      private _route: ActivatedRoute,
      private _hotelService: HotelService
  ) {
    
    this._route.params
        .subscribe(params => {
          this.cod_hotel = params.id;
        });

    this.formUpdateHotel = new FormGroup({
      data: new FormGroup({
        nombre: new FormControl('', [Validators.required]),
        ciudad: new FormControl('', [Validators.required]),
        cant_pisos: new FormControl('', [Validators.required]),
      })
    });

    this.getHotel();
  }

  ngOnInit() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.add("profile-page");
  }
  ngOnDestroy() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.remove("profile-page");
  }

  getHotel() {
    this._hotelService.getHotel(this.cod_hotel).subscribe((resp: any) => {
      if (resp.success){
        this.hotel = resp.hotel;
        this.formUpdateHotel.get('data.nombre').setValue(this.hotel.nombre);
        this.formUpdateHotel.get('data.ciudad').setValue(this.hotel.ciudad);
        this.formUpdateHotel.get('data.cant_pisos').setValue(this.hotel.cant_pisos);
      }
    }, error => {
      Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: error.error.message,
      });
    });
  }

  updateHotel() {
    const data = this.formUpdateHotel.value.data;

    this._hotelService.updateHotel(this.cod_hotel, data).subscribe((resp: any) => {

      if (resp.success){
        this.hotel = resp.hotel;
        Swal.fire({
          type: 'success',
          title: 'Éxito',
          text: resp.message,
        });
      }
    }, error => {
      Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: error.error.message,
      });
    });
  }

}
