import { Component, OnInit, OnDestroy } from "@angular/core";
import {HotelService} from "../../../services/hotel/hotel.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import Swal from "sweetalert2";

@Component({
  selector: "app-createhotel",
  templateUrl: "createhotel.component.html"
})
export class CreatehotelComponent implements OnInit, OnDestroy {
  
  isCollapsed = true;
  formCreateHotel: FormGroup;
  
  constructor(
      private _hotelService: HotelService
  ) {
    this.formCreateHotel = new FormGroup({
      data: new FormGroup({
        nombre: new FormControl('', [Validators.required]),
        ciudad: new FormControl('', [Validators.required]),
        cant_pisos: new FormControl('', [Validators.required]),
      })
    });
  }

  ngOnInit() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.add("profile-page");
  }
  ngOnDestroy() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.remove("profile-page");
  }

  createHotel() {
    const data = this.formCreateHotel.value.data;

    this._hotelService.createHotels(data).subscribe((resp: any) => {
      if (resp.success){
        this.formCreateHotel.reset({});
        Swal.fire({
          type: 'success',
          title: 'Éxito',
          text: resp.message,
        });
      }
    }, error => {
      Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: error.error.message,
      });
    });
  }
}
