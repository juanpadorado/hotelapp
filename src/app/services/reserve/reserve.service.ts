import { HttpClient, HttpHeaders } from '@angular/common/http';
import {GLOBAL} from "../global";
import {Injectable} from "@angular/core";

@Injectable({
    providedIn: 'root'
})

export class ReserveService {

    constructor(private http: HttpClient) {}

    createReserve(data: any) {
        return this.http.post<any>(GLOBAL.http + GLOBAL.routeCreateReserve, data);
    }

    listReserves() {
        return this.http.get<any>(GLOBAL.http + GLOBAL.routeListReserves);
    }

    getReserve(id) {
        return this.http.get<any>(GLOBAL.http + GLOBAL.routeGetReserve + id);
    }

}
