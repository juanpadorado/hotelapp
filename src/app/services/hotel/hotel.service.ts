import { HttpClient, HttpHeaders } from '@angular/common/http';
import {GLOBAL} from "../global";
import {Injectable} from "@angular/core";

@Injectable({
    providedIn: 'root'
})

export class HotelService {

    constructor(private http: HttpClient) {}

    createHotels(data: any) {
        return this.http.post<any>(GLOBAL.http + GLOBAL.routeHotel, data);
    }

    listHotels() {
        return this.http.get<any>(GLOBAL.http + GLOBAL.routeHotel);
    }

    activeHotel(data: any) {
        return this.http.post<any>(GLOBAL.http + GLOBAL.routeActiveHotel, data);
    }

    getHotel(id) {
        return this.http.get<any>(GLOBAL.http + GLOBAL.routeHotelShow + id);
    }

    updateHotel(id, data: any) {
        return this.http.put<any>(GLOBAL.http + GLOBAL.routeUpdateHotel + id, data);
    }

}
