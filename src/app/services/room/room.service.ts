import { HttpClient, HttpHeaders } from '@angular/common/http';
import {GLOBAL} from "../global";
import {Injectable} from "@angular/core";

@Injectable({
    providedIn: 'root'
})

export class RoomService {

    constructor(private http: HttpClient) {}

    createRoom(data: any) {
        return this.http.post<any>(GLOBAL.http + GLOBAL.routeCreateRoom, data);
    }

    listRooms(id) {
        return this.http.get<any>(GLOBAL.http + GLOBAL.routeRoom + id);
    }

    activeRoom(data: any) {
        return this.http.post<any>(GLOBAL.http + GLOBAL.routeActiveRoom, data);
    }

    getRoom(id) {
        return this.http.get<any>(GLOBAL.http + GLOBAL.routeRoomShow + id);
    }

    updateRoom(id, data: any) {
        return this.http.put<any>(GLOBAL.http + GLOBAL.routeUpdateRoom + id, data);
    }

    searchRoom(data: any) {
        return this.http.post<any>(GLOBAL.http + GLOBAL.routeSearchRoom, data);
    }

}
