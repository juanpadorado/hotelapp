export let GLOBAL = {
    limit: 10,
    routeApi: 'api/api/',
    http: 'https://hotelapp.azurewebsites.net/api/',
    routeLogin: 'login',

    routeHotel: 'hotels',
    routeActiveHotel: 'activeHotel',
    routeHotelShow: 'hotel/',
    routeUpdateHotel: 'hotels/',

    routeRoom: 'rooms/',
    routeCreateRoom: 'room',
    routeActiveRoom: 'activeRoom',
    routeRoomShow: 'getRoom/',
    routeUpdateRoom: 'updateRoom/',
    routeSearchRoom: 'searchRoom',

    routeCreateReserve: 'createReserve',
    routeListReserves: 'reserves',
    routeGetReserve: 'getReserve/',
};
